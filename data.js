let dataJoueur = [];
let dataJeux = [];
let nomEquipe = [];

exports.getJoueur = () => {
    return dataJoueur;
};

exports.getJeux = () => {
    return dataJeux;
};

exports.addJoueur = (nom) => {
    dataJoueur.push(nom);
    return 200;
}

exports.addJeu = (jeu) => {
    dataJeux.push(jeu);
    return 200;
}

exports.deleteNom = () => {
    dataJoueur = [];
    return 200;
}

exports.deleteJeu = () => {
    dataJeux = [];
    return 200;
}

exports.deleteEquipe = () => {
    nomEquipe = [];
    return 200;
}

exports.addEquipe = (nom) => {
    nomEquipe.push(nom);
    return 200;
}

exports.getEquipe = () => {
    return nomEquipe;
}