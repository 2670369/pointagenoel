const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const bodyParser = require('body-parser');
const serveStatic = require('serve-static');
const cors = require('cors');
const data = require('./data');

/** Port pour le serveur */
const PORT = process.env.PORT;

/** Création du serveur */
let app = express();


// Mettre des options différentes à Helmet en développement
let helmetOptions = null;
if(app.settings.env === 'development'){
    helmetOptions = require('./developement-csp');
}

// Ajout de middlewares
app.use(helmet(helmetOptions));
app.use(compression());
app.use(cors());
app.use(bodyParser.json({ strict: false }));
app.use(serveStatic('./public'));

// Ajouter vos routes ici
app.post('/addJoueur', (request, response) => {
    response.sendStatus(200);
    data.addJoueur(request.body);
});

app.post('/nomJeu', (request, response) => {
    response.sendStatus(200);
    data.addJeu(request.body)
});

app.get('/getJoueur', async (request, response) => {
    let reponse = await data.getJoueur();
    response.status(200).json(reponse);
});

app.get('/getJeu', async (request, response) => {
    let reponse = await data.getJeux();
    response.status(200).json(reponse);
});

app.post('/deleteNom', (request, response) => {
    response.sendStatus(200);
    data.deleteNom()
});

app.post('/deleteJeu', (request, response) => {
    response.sendStatus(200);
    data.deleteJeu()
});

app.post('/addEquipe', (request, response) => {
    response.sendStatus(200);
    data.addEquipe(request.body)
});

app.get('/getEquipe', async (request, response) => {
    let reponse = await data.getEquipe();
    response.status(200).json(reponse);
});

app.post('/deleteEquipe', (request, response) => {
    response.sendStatus(200);
    data.deleteEquipe()
});

// Renvoyer une erreur 404 pour les routes non définies
app.use(function (request, response) {
    // Renvoyer simplement une chaîne de caractère indiquant que la page n'existe pas
    response.status(404).send(request.originalUrl + ' not found.');
});

// Démarrage du serveur
app.listen(PORT);
