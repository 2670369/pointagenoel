var listeNom = document.getElementById('body');
var listJeux = document.getElementById('jeux');
var listTitres = document.getElementById('titres');
var formScore = document.getElementById('form-score');
var block = document.getElementById('block');
var terminer = document.getElementById('terminer');
let template = document.getElementById('creation-form');
let form = document.getElementById('form')
let tableEquipe = document.getElementById('table-equipe')
let randomize = document.getElementById('randomize')

let noms = [];
let jeux = [];
let equipe = [];
let idEquipe = [];
let totalPoints = 0;

function formJeu(id) {
    let random = noms.sort(() => Math.random() - 0.5);
    formScore.style.display = "flex"
    block.style.display = "block"
    form.innerHTML = ""
    for (i = 0; i < noms.length; i++) {
        let copy = template.content.firstElementChild.cloneNode(true);
        copy.querySelector(".nom").value = random[i];
        form.append(copy)
    }
    document.getElementById(id).disabled = true;
};

function totalJoueur(id, check) {
    return () => {
        var checkbox = document.getElementById(check)
        let nomJoueur = listeNom.getElementsByClassName('nom')
        for (i = 0; i < nomJoueur.length; i++) {
            if (id === nomJoueur[i].innerHTML) {
                var totals = document.getElementById(id)
                let ajout = parseInt(totals.innerHTML)
                if (checkbox.checked) {
                    ajout += 1
                } else {
                    ajout -= 1
                }
                totals.innerHTML = ajout
            }
        }
    }
}

function plusPoint(id) {
    return () => {
        let titre = tableEquipe.getElementsByClassName('titre')
        for (i = 0; i < titre.length; i++) {
            if (id === titre[i].innerHTML) {
                var point = document.getElementsByClassName(id);
                for (j = 0; j < point.length; j++) {
                    let ajout = parseInt(point[j].innerHTML)
                    ajout += 1
                    point[j].innerHTML = ajout
                }
            }
        }
    }
}

function moinsPoint(id) {

    return () => {
        let titre = tableEquipe.getElementsByClassName('titre')
        for (i = 0; i < titre.length; i++) {
            if (id === titre[i].innerHTML) {
                var cat = document.getElementById(id);
                var point = cat.getElementsByClassName(id);
                for (j = 0; j < point.length; j++) {
                    let ajout = parseInt(point[j].innerHTML)
                    if (!ajout <= 0) {
                        ajout -= 1
                    }
                    point[j].innerHTML = ajout
                }

            }
        }
    }
}

randomize.onclick = function randomizeBtn() {
    let randomize = noms.sort(() => Math.random() - 0.5);

    formScore.style.display = "flex"
    block.style.display = "block"
    form.innerHTML = ""
    for (i = 0; i < noms.length; i++) {
        let copy = template.content.firstElementChild.cloneNode(true);
        copy.querySelector(".nom").value = randomize[i];

        form.append(copy)
    }
}

terminer.onclick = function () {
    formScore.style.display = "none"
    block.style.display = "none"
};

(async () => {

    let resultatEquipe = await fetch("/getEquipe");
    if (resultatEquipe.ok) {
        equipe = await resultatEquipe.json();
    }

    let resultat = await fetch("/getJoueur");
    if (resultat.ok) {
        noms = await resultat.json();
    }

    let resultatJeu = await fetch("/getJeu");
    if (resultatJeu.ok) {
        jeux = await resultatJeu.json();
    }

    let idJ = null
    for (i = 0; i < noms.length; i++) {
        var creation = document.createElement('tr');
        var nom = document.createElement('td')
        var total = document.createElement('td')
        idJ = noms[i]
        total.innerText = totalPoints;
        total.id = idJ
        nom.innerText = idJ
        nom.classList = "nom"
        listeNom.appendChild(creation);
        creation.appendChild(nom)
        creation.appendChild(total)
        for (j = 0; j < jeux.length; j++) {
            var points = document.createElement('td')
            var input = document.createElement('input')
            input.type = 'checkbox'
            input.id = idJ + j
            input.addEventListener("click", totalJoueur(idJ, input.id));
            points.appendChild(input);
            creation.appendChild(points);
        }
    }

    for (i = 0; i < jeux.length; i++) {
        var creation = document.createElement('button');
        var titres = document.createElement('th');
        titres.innerText = jeux[i]
        creation.innerText = jeux[i];
        creation.id = jeux[i];
        creation.onclick = function () { formJeu(this.id) };
        listJeux.appendChild(creation);
        listTitres.appendChild(titres)
    }

    let idE = null

    for (i = 0; i < equipe.length; i++) {
        var equipeCat = document.createElement('td')
        var titre = document.createElement('p');
        var points = document.createElement('div')
        var plus = document.createElement('button')
        var moins = document.createElement('button')
        idE = equipe[i]
        titre.innerText = idE
        equipeCat.id = idE
        titre.classList = "titre"
        points.innerText = 0
        points.classList = idE
        plus.innerText = "+"
        plus.addEventListener("click", plusPoint(idE));
        moins.addEventListener("click", moinsPoint(idE));
        moins.innerText = "-"
        tableEquipe.appendChild(equipeCat);
        equipeCat.appendChild(titre);
        equipeCat.appendChild(points);
        equipeCat.appendChild(plus);
        equipeCat.appendChild(moins);
    }
})();