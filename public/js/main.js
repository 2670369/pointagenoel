var participant = document.getElementById('participant');
var nomParticipant = document.getElementById('nom-participant');
var jeux = document.getElementById('jeux');
var nomJeu = document.getElementById('nom-jeux');
var nomEquipe = document.getElementById('nom-equipe');
var suivant = document.getElementById('suivant');
var famille = document.getElementById('famille');
var montantFamille = document.getElementById('montant-famille');
var nombre = document.getElementById('nombre');

var nomJeux = [];
var nomParticipants = [];
var nomsEquipe = [];

participant.onchange = function () {
    nomParticipant.innerHTML = ""
    for (i = 0 ; i < participant.value ; i++){
        var nomP = document.createElement('input');
        nomP.id = "nomP" + i
        nomParticipant.append(nomP);
    }
}

jeux.onchange = function () {
    nomJeu.innerHTML = ""
    for (i = 0 ; i < jeux.value ; i++){
        var nomJ = document.createElement('input');
        nomJ.id = "nomJ" + i
        nomJeu.append(nomJ);
    }
}

famille.onchange = function () {
    if (famille.checked === true){
        montantFamille.style.display = "flex"
    } else {
        montantFamille.style.display = "none"
        nombre.value = 0
        nomEquipe.innerHTML = ""
    }
}

nombre.onchange = function () {
    nomEquipe.innerHTML = ""
    for (i = 0 ; i < nombre.value ; i++){
        var nomE = document.createElement('input');
        nomE.id = "nomE" + i
        nomEquipe.append(nomE);
    }
}

suivant.onclick =  async function () {

    var participantInput = nomParticipant.querySelectorAll("input");
    var jeuxInput = nomJeu.querySelectorAll("input");
    var equipeInput = nomEquipe.querySelectorAll("input");

    fetch('/deleteJeu', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    });
    fetch('/deleteNom', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    });
    fetch('/deleteEquipe', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    });

    for (i = 0 ; i < equipeInput.length ; i++){
        fetch('/addEquipe', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(equipeInput[i].value)
        });
    }


    for (i = 0 ; i < participantInput.length ; i++){
        fetch('/addJoueur', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(participantInput[i].value)
        });
    }
    
    for (i = 0 ; i < jeuxInput.length ; i++){
        fetch('/nomJeu', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(jeuxInput[i].value)
        });
    }
    
    window.location.href = "scoreBoard.html"

}